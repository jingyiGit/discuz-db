<?php

namespace DiscuzDb\db;

use DiscuzDb\db\BaseQuery;
use DiscuzDb\builder\Mysql;

/**
 * Class DbManager
 *
 * @package DiscuzDb
 * @mixin BaseQuery
 * @mixin Query
 */
class DbManager extends Query
{
  /**
   * 架构函数
   *
   * @access public
   */
  public function __construct($config = [])
  {
    $this->pk    = $config['pk'];
    $this->table = $config['table'];
    if ($config['prefix']) {
      $this->prefix = $config['prefix'];
    }
    if (!empty($config['schema'])) {
      $this->setFieldType($config['schema']);
    }
    if ($config['getSql']) {
      $this->stop();
    }
    $this->table($this->table);
    parent::__construct();
  }
  
  /**
   * 使用表达式设置数据
   *
   * @access public
   * @param string $value 表达式
   * @return Raw
   */
  public function raw(string $value): Raw
  {
    return new Raw($value);
  }
}
