<?php
spl_autoload_register(function ($class) {
  if (strpos($class, '\\model\\') !== false) {
    $file = str_replace('\\', '/', DISCUZ_ROOT . 'source/plugin/' . $class . '.php');
    if (file_exists($file)) {
      include_once $file;
    }
  }
}, true);
