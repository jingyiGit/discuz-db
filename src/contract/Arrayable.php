<?php

namespace DiscuzDb\contract;

interface Arrayable
{
    public function toArray(): array;
}
